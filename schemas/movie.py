
from pydantic import BaseModel, Field
from typing import Optional

class Movie(BaseModel):
        id: Optional[int] = None
        title: str = Field(min_length=1,max_length=30)
        overview: str= Field(min_length=15,max_length=50)
        year: int = Field(xe=1950)
        rating: float = Field(le=50)
        category: str = Field(min_length=5,max_length=15)

        #clase default para el esquema de valores
class Config:
                schema_extra = {
                        "example":{
                                "id":1,
                                "title":"Mi pelicula",
                                "overview":"Descripcion de la pelicula",
                                "year":2022,
                                "rating":9.8,
                                "category":"Accion"
                        }

                }

movies = [
{
					"id":1,
					"title": "avatar",
					"overview":"en un exuberante planeta se comen a todos",
					"year":2010,
					"rating":7.8,
					"category":"comedia"
					
		},
                {
					"id":2,
					"title": "avatar 2",
					"overview":"en un exuberante planeta se comen a todos",
					"year":2009,
					"rating":8,
					"category":"accion"
					
		},
                 {
					"id":3,
					"title": "avatar 3",
					"overview":"en un exuberante planeta se comen a todos",
					"year":2009,
					"rating":8,
					"category":"accion"
					
		}
]