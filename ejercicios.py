def max_earnings(earnings_per_fight):
    num_fights = len(earnings_per_fight)
    
    if num_fights == 0:
        return 0
    if num_fights == 1:
        return 0
    
    dp = [0] * num_fights
    dp[0] = earnings_per_fight[0]
    dp[1] = earnings_per_fight[1]

    for i in range(2, num_fights):
        dp[i] = max(earnings_per_fight[i] + dp[i - 2], dp[i - 1])

    return dp[num_fights - 1]



# Definir la lista de ganancias por pelea
earnings_per_fight = [10, 12, 5, 2,50]

# Llamar a la función max_earnings con la lista definida
resultado = max_earnings(earnings_per_fight)

# Imprimir el resultado
print("El máximo que puedes ganar es:", resultado)
