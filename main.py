from fastapi import FastAPI
from fastapi.responses import HTMLResponse, JSONResponse
from pydantic import BaseModel
from config.database import engine, Base
# para convetir un modelo a un objeto
from middlewares.error_handler import ErrorHandler
from routers.movie import movie_router
from routers.user import user_router
from utils.jwt_manager import create_token




app = FastAPI()
#asii modifico el titulo en la documentacion
app.title = "mi aplicacion "
#asi modifico la version 
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(movie_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)



@app.get('/',tags=['severoooo'])
def message():
    return HTMLResponse('<H1>hola MUNDO MODIFICADO</H2>')


        



         