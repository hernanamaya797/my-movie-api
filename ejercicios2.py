def buying_candy(amount_of_money):
    if amount_of_money <= 0:
        return 0
    
    dp = {0: 1, 1: 1}
    
    for x in range(2, amount_of_money + 1):
        dp[x] = dp.get(x - 1, 0) + dp.get(x - 2, 0)
        
    return dp[amount_of_money]


res= buying_candy(5)
print("el resultado es =",res)