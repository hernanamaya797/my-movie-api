from fastapi import APIRouter
from fastapi import Depends, Path, Query
from fastapi.responses import  JSONResponse
from pydantic import BaseModel, Field
from typing import Optional, List
from config.database import Session
from models.movie import Movie as MovieModel
# para convetir un modelo a un objeto
from fastapi.encoders import jsonable_encoder
from middlewares.jwt_bearer import JWTBearer
from services.movie import MovieService
from schemas.movie import Movie

movie_router = APIRouter()


#agregamos la funion list de typin y podemos obtener una lista, agregando reponse_model en la ruta
@movie_router.get('/movies',tags=['movies_database'], response_model=List[Movie], status_code=200, dependencies=[Depends(JWTBearer())])
def get_movies() -> List[Movie]:
        db= Session()
        result= db.query(MovieModel).all()
        return JSONResponse(status_code=200,content=jsonable_encoder(result))


@movie_router.get('/movies/{id}', tags=["movies_database_for_id"], response_model=Movie)
def get_movie(id :int = Path(ge=1, le=2000)) -> Movie:
        db= Session()
     #   result= db.query(MovieModel).filter(MovieModel.id == id).first()
        result= MovieService(db).get_movies(id)
        if not result:
             return JSONResponse(status_code=404, content={'message':'no encontrado ni damier'})
        return JSONResponse(status_code=200, content=jsonable_encoder(result))
'''''''''''''''
@movie_router.get('/movies2/{id}', tags=["movies"])
def get_movie(id :int = Path(ge=1, le=2000)):
			for item in movies:
				if item["id"] == id:
					return JSONResponse(content=item)
			return JSONResponse(status_code=404, content=[])	

   #parametros tipo query
@movie_router.get('/movies/', tags=["movies"])
def get_movie_by_year(year: str, id: int):
    for item in movies:    
        if item["year"] == year:
            if item["id"] == id:
                return item
    return []


@movie_router.get('/moviesv/', tags=["movies"])
def get_movie_by_category(category: str= Query(min_length=5, max_length=15)) -> List[Movie]:
    
        return [ item for item in movies if item["category"] == category]
'''''
@movie_router.get('/movies/', tags=["movies_category_database_services"],response_model=List[Movie])
def get_movie_by_category(category: str= Query(min_length=5, max_length=15)) -> List[Movie]:
        db = Session()
       # result=db.query(MovieModel).filter(MovieModel.category == category).all()
        result= MovieService(db).get_movies_by_category(category)
      #  if not result:
       #         return JSONResponse(status_code=404, content={'message': 'categoria no encontrada'})
        return JSONResponse(status_code=200, content=jsonable_encoder(result))

'''''
#metodo Post

@movie_router.post('/movies', tags=["movies_post"])
def create_movie_post(movie: Movie):
        movies.movie_routerend(movie)
        return movies
'''''        
# Usando la base de datos
@movie_router.post('/moviesJason', tags=["movies_post_database_for_services"], response_model=dict, status_code=200)
def create_movie_post(movie: Movie) -> dict:
        db= Session()
        MovieService(db).create_movie(movie)
    #    movies.movie_routerend(movie) como voy a agregarlo directamente a la bd no la necesito
        return JSONResponse(status_code=201,content={"message":"Pelicula registrada"})
'''''
#Metodo PUT y DELETE

@movie_router.put('/movies/{id}', tags=['movies'], response_model=dict)
def update_movie(id: int,movie: Movie)-> dict:
#el id como se recibe de otra manera no sera necesario recibirlo en el body
	for item in movies:
                if item["id"] == id:
                   item['title'] =movie.title
                   item['overview'] = movie.overview
                   item['year'] = movie.year
                   item['rating'] = movie.rating
                   item['category'] = movie.category
                   return movies
'''''                
@movie_router.put('/moviesjason/{id}', tags=['movies_actualizar_database'], response_model=dict, status_code=200)
def update_movie(id: int,movie: Movie) -> dict:
#el id como se recibe de otra manera no sera necesario recibirlo en el body
    db= Session()
    result=MovieService(db).get_movies(id)
    if not result:
              return JSONResponse(status_code=404, content={'message': 'Pelicula no encontrada'})
    MovieService(db).update_movie(id, movie)
   # result.title= movie.title
   # result.overview= movie.overview
   # result.year= movie.year
   # result.rating= movie.rating
   # result.category= movie.category
    db.commit()
    return JSONResponse(content={"message":"se modifico la pelicula"})               
'''''                
@movie_router.delete('/moviesdelete/{id}',tags=['movies'], response_model=dict)
def delete_movie(id: int)-> dict:
        for item in movies:
                if item["id"]== id:
                        movies.remove(item)
                        return movies
'''''                                
@movie_router.delete('/movies/{id}',tags=['movies_delete_for_id_database'], response_model=dict, status_code=200) 
def delete_movie(id: int) -> dict:
    db= Session()
    result:MovieModel = db.query(MovieModel).filter(MovieModel.id== id).first()
    if not result:
              return JSONResponse(status_code=404, content={'message': 'pelicula no encontrada'})   
    MovieService(db).delete_movie(id)
    return JSONResponse(status_code=200, content={"message":"se elimino la pelicula"}) 